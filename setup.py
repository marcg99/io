# SPDX-License-Identifier: LGPL-3.0-or-later

from setuptools import setup, find_packages


entry_points = {
    "console_scripts": [
        "robofish-io-validate=robofish.io.app:validate",
        "robofish-io-print=robofish.io.app:print",
        # TODO: This should be called robofish-evaluate which is not possible because of the package name (guess) ask moritzs
        "robofish-io-evaluate=robofish.evaluate.app:evaluate",
    ]
}
setup(
    name="robofish-io",
    version="0.1",
    author="",
    author_email="",
    install_requires=["h5py>=3", "numpy"],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
    ],
    python_requires=">=3.7",
    packages=find_packages("src"),
    package_dir={"": "src"},
    zip_safe=True,
    entry_points=entry_points,
)
