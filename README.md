
<div align="center">
<a href="https://git.imp.fu-berlin.de/bioroboticslab/robofish/io/">
  <img alt="RobofishIO" title="RobofishIO" src="img/logo45.svg" width="200">
</a>
</div>

[![pipeline status](https://git.imp.fu-berlin.de/bioroboticslab/robofish/io/badges/master/pipeline.svg)](https://git.imp.fu-berlin.de/bioroboticslab/robofish/io/commits/master)
# Robofish IO

This repository implements an easy to use interface, to create, save, load, and work  [specification-compliant](https://git.imp.fu-berlin.de/bioroboticslab/robofish/track_format) hdf5 files, containing 2D swarm data. This repository should be used by the different swarm projects to generate comparable standardized files.


## Installation

Clone and install the repository:

```
git clone https://git.imp.fu-berlin.de/bioroboticslab/robofish/io.git
pip install ./io
```

## Usage
We show a simple example below. More examples can be found in ```examples/```

```python
import robofish.io
import numpy

f = robofish.io.File(world_size=[100, 100])

# Create a single robot with 40 poses
# (x,y, x_orientation, y_orientation) and specified time points
f.create_single_entity(
    type_="robot",
    name="robot",
    poses=numpy.zeros((40, 4)),
    monotonic_points=range(0, 800, 20),
)

# Create 2 fishes with 100 poses
# (x,y, x_orientation, y_orientation) and 40ms timesteps
f.create_multiple_entities("fish", poses=numpy.zeros((2, 100, 4)), monotonic_step=40)

# Show and save the file
print(f)
f.save("example.hdf5")
```


## LICENSE

This work is licensed under LGPL 3.0 (or any later version).
Individual files contain the following tag instead of the full license text:

`SPDX-License-Identifier: LGPL-3.0-or-later`

This enables machine processing of license information based on the SPDX License Identifiers available here: https://spdx.org/licenses/
