import robofish.io
import numpy

f = robofish.io.File(world_size=[100, 100])

# Create a single robot with 40 poses
# (x,y, x_orientation, y_orientation) and specified time points
f.create_single_entity(
    type_="robot",
    name="robot",
    poses=numpy.zeros((40, 4)),
    monotonic_points=range(0, 800, 20),
)

# Create 2 fishes with 100 poses
# (x,y, x_orientation, y_orientation) and 40ms timesteps
f.create_multiple_entities("fish", poses=numpy.zeros((2, 100, 4)), monotonic_step=40)

# Show and save the file
print(f)
f.save("example.hdf5")
