#! /usr/bin/env python3

import robofish.io
import numpy as np
from pathlib import Path
import os


# Helper function to enable relative paths from this file
def full_path(path):
    return (Path(os.path.abspath("__file__")).parent / path).resolve()


if __name__ == "__main__":
    # Create a new io file object with a 100x100cm world
    sf = robofish.io.File(world_size=[100, 100])

    # create a simple obstacle, fixed in place, fixed outline
    obstacle_pose = [[50, 50, 0, 0]]
    obstacle_outline = [[[-10, -10], [-10, 0], [0, 0], [0, -10]]]
    obstacle_name = sf.create_single_entity(
        "obstacle", poses=obstacle_pose, outlines=obstacle_outline
    )

    # create a robofish with 100 timesteps and 40ms between the timesteps. If we would not give a name, the name would be generated to be robot_1.
    robofish_timesteps = 4
    robofish_poses = np.zeros((robofish_timesteps, 4))
    sf.create_single_entity("robot", robofish_poses, name="robot", monotonic_step=40)

    # create multiple fishes with timestamps. Since we don't specify names, but only the type "fish" the fishes will be named ["fish_1", "fish_2", "fish_3"]
    agents = 3
    timesteps = 5
    timestamps = np.linspace(0, timesteps + 1, timesteps)
    agent_poses = np.random.random((agents, timesteps, 4))

    fish_names = sf.create_multiple_entities(
        "fish", agent_poses, monotonic_points=timestamps
    )

    # This would throw an exception if the file was invalid
    sf.validate()

    # Save file validates aswell
    example_file = full_path("example.hdf5")
    sf.save(example_file)

    # Closing and opening files (just for demonstration)
    sf.close()
    sf = robofish.io.File(path=example_file)

    print("\nEntity Names")
    print(sf.get_entity_names())

    # Get an array with all poses. As the length of poses varies per agent, it
    # is filled up with nans. The result is not interpolated and the time scales
    # per agent are different. It is planned to create a warning in the case of
    # different time scales and have another function, which generates an
    # interpolated array.
    print("\nAll poses")
    print(sf.get_poses_array())

    print("\nFish poses")
    print(sf.get_poses_array(fish_names))

    print("\nFile structure")
    print(sf)
