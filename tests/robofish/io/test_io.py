import robofish.io
import numpy as np
from pathlib import Path
import pytest
import inspect
import datetime
import sys

#### Helpers ####
def full_path(path):
    return (Path(__file__).parent / path).resolve()


def test_constructor():
    sf = robofish.io.File(world_size=[100, 100])
    print(sf)
    sf.validate()


def test_missing_attribute():
    sf = robofish.io.File(world_size=[10, 10])
    sf.attrs.pop("world size")
    assert not sf.validate(strict_validate=False)[0]


def test_single_entity_monotonic_step():
    sf = robofish.io.File(world_size=[100, 100])
    test_poses = np.ones(shape=(10, 4))
    sf.create_single_entity("robofish", poses=test_poses, monotonic_step=40)
    print(sf)
    sf.validate()


def test_single_entity_monotonic_points():
    sf = robofish.io.File(world_size=[100, 100])
    test_poses = np.ones(shape=(10, 4))
    sf.create_single_entity("robofish", poses=test_poses, monotonic_points=np.ones(10))
    print(sf)
    sf.validate()


# def test_single_entity_global_monotonic_step():
#     sf = robofish.io.File([100, 100], monotonic_step=10)
#
#     sf.create_single_entity("fish", np.ones(shape=(10, 4)))
#     sf.validate()


def test_multiple_entities():
    agents = 3
    timesteps = 10

    poses = np.zeros((agents, timesteps, 4))
    poses[1] = 1
    poses[2] = 2

    m_points = np.arange(timesteps)

    sf = robofish.io.File(world_size=[100, 100])
    returned_names = sf.create_multiple_entities(
        "fish", poses, monotonic_points=m_points
    )

    expected_names = ["fish_1", "fish_2", "fish_3"]
    print(returned_names)
    assert returned_names == expected_names
    print(sf)

    sf.validate()

    # The returned poses should be equal to the inserted poses
    returned_poses = sf.get_poses_array()
    print(returned_poses)
    assert (returned_poses == poses).all()

    # Just get the array for some names
    returned_poses = sf.get_poses_array(["fish_1", "fish_2"])
    assert (returned_poses == poses[:2]).all()

    # Insert some random obstacles
    returned_names = sf.create_multiple_entities(
        "obstacle",
        poses=np.random.random((agents, timesteps, 4)),
        monotonic_points=m_points,
    )
    # Obstacles should not be returned when only fish are selected
    returned_poses = sf.get_poses_array(type_="fish")
    assert (returned_poses == poses).all()

    # for each of the entities
    outlines = np.ones((agents, timesteps, 20, 2))
    m_points = np.ones((agents, timesteps))

    # Create Array
    c_points = np.empty((agents, timesteps), dtype="O")
    c_points[:] = "2020-12-02T10:21:58.100+00:00"

    c_points[0, :] = robofish.io.now_iso8061()

    returned_names = sf.create_multiple_entities(
        "fish",
        poses,
        monotonic_points=m_points,
        outlines=outlines,
        calendar_points=c_points,
    )
    print(returned_names)
    print(sf)
    sf.validate()


def test_load_validate():
    sf = robofish.io.File(path=full_path("../../resources/valid.hdf5"))
    print(sf)
    sf.validate()


def test_loading_saving():
    almost_valid_path = full_path("../../resources/almost_valid.hdf5")
    created_by_test_path = full_path("../../resources/created_by_test.hdf5")

    sf = robofish.io.File(path=almost_valid_path)

    # Cannot be saved as it it not valid
    with pytest.raises(AssertionError):
        sf.save(created_by_test_path)
    assert not created_by_test_path.exists()

    # correct the error
    world_size = sf.attrs.pop("world_size")
    sf.attrs["world size"] = world_size

    print(sf)
    sf.validate()
    sf.save(created_by_test_path)
    assert created_by_test_path.exists()

    # After saving, the file should still be accessible and valid
    sf.validate()


def test_validate_new():
    sf = robofish.io.File(path=full_path("../../resources/created_by_test.hdf5"))
    sf.validate()


# Cleanup test. The z in the name makes sure, that it is executed last in main
def test_z_cleanup():
    """ This cleans up after all tests and removes all test artifacts """
    full_path("../../resources/created_by_test.hdf5").unlink()


if __name__ == "__main__":
    # Find all functions in this module and execute them
    all_functions = inspect.getmembers(sys.modules[__name__], inspect.isfunction)
    for key, value in all_functions:
        if str(inspect.signature(value)) == "()":
            value()
