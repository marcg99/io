import robofish.io
import pytest
import numpy as np
import logging


logging.getLogger().setLevel(logging.INFO)


def test_validate_iso8601():
    """
    This tests the function utils.validate_iso8601 with valid and invalid examples.
    """
    valid_dates = ["2020-12-02T10:21:58.100+00:00"]

    invalid_dates = [
        "2020-12-02T10:21:58+00:00",
        "2020-12-02T10:21:58Z",
        "2020-12-02",
        "2020-W49",
        "2020-W49-3",
        "2020-337",
    ]

    for date in valid_dates:
        print("Testing valid date %s" % date)
        assert robofish.io.validate_iso8601(date)

    for date in invalid_dates:
        print("Testing invalid date %s" % date)
        assert not robofish.io.validate_iso8601(date)
