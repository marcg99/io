import robofish.io.app as app
import pytest
import logging
from pathlib import Path

logging.getLogger().setLevel(logging.INFO)


#### Helpers ####
def full_path(path):
    return (Path(__file__).parent / path).resolve()


def test_app_validate():
    """ This tests the function of the robofish-io-validate command """

    class DummyArgs:
        def __init__(self, path, output_format):
            self.path = path
            self.output_format = output_format

    raw_output = app.validate(DummyArgs(full_path("../../resources"), "raw"))

    # The three files valid.hdf5, almost_valid.hdf5, and invalid.hdf5 should be found.
    assert len(raw_output) == 3
    app.validate(DummyArgs(full_path("../../resources"), "human"))
