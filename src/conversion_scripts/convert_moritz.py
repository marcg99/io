# -*- coding: utf-8 -*-

# -----------------------------------------------------------
# This script converts the datasets from Moritz Maxeiners Masterthesis to
# the io file format. The original datasets are available at
# https://zenodo.org/record/3457834#.X9eC8MIo9FE
#
# Dec 2020 Andreas Gerken, Berlin, Germany
# Released under GNU 3.0 License
# email andi.gerken@gmail.com
# -----------------------------------------------------------

from pathlib import Path
import robofish.io
import h5py

try:
    from tqdm import tqdm
except ImportError as e:
    print("tqdm not installed, not showing progress bar.")

    def tqdm(x):
        return x


# Please configure to your local folder here
mm_data_folder = "/home/andi/phd_workspace/mm/mm_data"

input = Path(mm_data_folder) / "datasets"
output = Path(mm_data_folder) / "datasets_io/"

files = input.rglob("*.hdf5")
files = (list)(files)
for f in tqdm(files):
    # print("Handling file %s" % f.relative_to(input))

    old = h5py.File(f)
    world = old.attrs["world"] if "world" in old.attrs else [100, 100]
    new = robofish.io.File(world_size=world)

    new.attrs.update(
        {a: old.attrs[a] for a in old.attrs.keys() if a not in ["world", "time_step"]}
    )

    for e_name, e_data in old.items():
        new.create_single_entity(
            type_="fish",
            name=e_name,
            poses=e_data,
            monotonic_step=old.attrs["time_step"],
        )
    if not new.validate(strict_validate=False)[0]:
        print(
            f"File {input.relative_to(input)} could not be converted to a valid file. The error was:\n {new.validate(strict_validate=False)[1]}."
        )
        continue

    new.save(output / f.relative_to(input))
    # print("Saved converted file")
