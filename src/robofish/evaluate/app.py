# -*- coding: utf-8 -*-

# -----------------------------------------------------------
# Functions available to be used in the commandline to evaluate robofish.io files
#
# Dec 2020 Andreas Gerken, Berlin, Germany
# Released under GNU 3.0 License
# email andi.gerken@gmail.com
# -----------------------------------------------------------

import robofish.evaluate

import argparse


def evaluate(args=None):
    """This function can be used to print hdf5 files from the command line

    Returns:
        A human readable print of a given hdf5 file.
    """
    parser = argparse.ArgumentParser(description="TODO")

    parser.add_argument("analysis_type", type=str, choices=["speed"])
    parser.add_argument(
        "paths",
        type=str,
        nargs="+",
        help="The paths to io/hdf5 files. Multiple paths can be given which will be shown in different colors",
    )

    if args is None:
        args = parser.parse_args()

    if args.analysis_type == "speed":
        robofish.evaluate.evaluate.evaluate_speed(args.paths)
