import sys
import logging

import robofish.io

from robofish.evaluate.evaluate import *
import robofish.evaluate.app

# TODO: REMOVE
# logging.getLogger().setLevel(logging.INFO)

assert (3, 7) <= sys.version_info < (4, 0), "Unsupported Python version"
