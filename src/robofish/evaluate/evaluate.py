import robofish.evaluate
import robofish.io
import matplotlib.pyplot as plt
import seaborn as sns

import numpy as np


def evaluate_speed(paths):

    files_per_path = [robofish.io.read_multiple_files(p) for p in paths]
    speeds = []
    for files in files_per_path:
        path_speeds = []
        for p, file in files.items():
            poses = file.get_poses_array()
            for e_poses in poses:
                e_speeds = np.linalg.norm(np.diff(e_poses[:, :2], axis=0), axis=1)
                path_speeds.extend(e_speeds)

        speeds.append(path_speeds)

    print(len(speeds))
    max_len = max([len(speed) for speed in speeds])

    print(max_len)
    print([len(speed) for speed in speeds])
    # speeds = [speed[:max_len] for speed in speeds]

    plt.hist(speeds, bins=20, label=paths, density=True, range=[0, 1])
    # sns.displot(speeds, label=paths, stat="probability", kind="hist")
    # sns.displot(speeds[1], label=paths[1], stat="probability", kind="hist")
    plt.title("Agent speeds")
    plt.xlabel("Speed (cm/timestep)")
    plt.ylabel("Frequency")
    plt.ticklabel_format(useOffset=False)
    # plt.xscale("log", nonpositive="clip")
    plt.legend()
    plt.tight_layout()
    plt.show()
