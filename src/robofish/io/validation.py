import re
import h5py
import numpy as np
import logging


def assert_validate(statement, message, entity_name=None):
    if not statement:
        if entity_name:
            raise AssertionError("%s in entity %s" % (message, entity_name))
        else:
            raise AssertionError(message)


def validate(iofile, strict_validate=True):
    """Validate a file to the specification.

    The function compares a given track to the robofish track format specification:
    https://git.imp.fu-berlin.de/bioroboticslab/robofish/track_format
    First all specified arrays are formatted to be numpy arrays with the specified
    datatype. Then all specified shapes are validated. Lastly calendar points
    are validated to be datetimes according to ISO8601.

    Args:
        track (dict): A track as a dictionary
        strict_validate (bool): Throw an exception instead of just returning false.
    Returns:
        dict/bool: The function returns the validated track or False if the track is
                   invalid and strict_validate is False
    Throws:
        AssertionError: Whenlogging the track is invalid and strict_validate is True
    """

    # TODO: Check dtypes
    array_dtypes = {
        "root": {"version": np.int32, "world size": np.float32},
        "entity": {"poses": np.float32, "outlines": np.float32},
        "entity_time": {"monotonic points": np.int64, "calendar points": "S"},
    }

    outlines = None

    try:

        #### Validation ####

        for a, a_type in array_dtypes["root"].items():
            assert_validate(a in iofile.attrs, f'Attribute "{a}" missing in root')
            assert_validate(
                iofile.attrs[a].dtype == a_type,
                f'The type of attribute "{a}" should be "{a_type}" but was "{iofile.attrs[a].dtype}" in root',
            )

        for e_name, e_data in iofile["entities"].items():

            assert_validate(
                "type" in e_data.attrs and isinstance(e_data.attrs["type"], str),
                'Attribute "type" not found',
                e_name,
            )

            for a, a_type in array_dtypes["entity"].items():
                if a in e_data:
                    assert_validate(
                        e_data[a].dtype == a_type,
                        f'The type of dataset "{a}" should be "{a_type}" but was "{e_data[a].dtype}" in root',
                        e_name,
                    )

            assert_validate(
                "poses" in e_data and isinstance(e_data["poses"], h5py.Dataset),
                'Dataset "poses" not found',
                e_name,
            )

            poses = e_data["poses"]
            assert_validate(
                poses.ndim == 2, "Dimensionality of poses should be 2", e_name
            )
            assert_validate(
                poses.shape[1] == 4,
                "The second dimension of poses should have the length 4",
                e_name,
            )

            # outlines
            if "outlines" in e_data:
                outlines = e_data["outlines"]

                assert_validate(
                    outlines.ndim == 3, "Dimensionality of outlines should be 3", e_name
                )

                # Either fixed outline or same length with poses
                assert_validate(
                    outlines.shape[0] == 1 or outlines.shape[0] == poses.shape[0],
                    "The outline has to be either fixed or it has to have the same length as poses",
                )

                # Outline from two dimensional points
                assert_validate(
                    outlines.shape[2] == 2,
                    "The third dimension of outlines should have the length 3",
                    e_name,
                )

            # time
            assert_validate(
                "time" in e_data and isinstance(e_data["time"], h5py.Group),
                'Group "time" not found',
                e_name,
            )

            e_time = e_data["time"]
            if "monotonic step" in e_time.attrs:
                m_step_type = type(e_time.attrs["monotonic step"])
                assert_validate(
                    m_step_type == np.int32,
                    f'The type of the attribute "monotonic step" should be np.int32 but was {m_step_type}',
                    e_name,
                )
            elif "monotonic points" in e_time:
                monotonic_points = e_time["monotonic points"]
                assert_validate(
                    isinstance(monotonic_points, h5py.Dataset),
                    "Monotonic points was found but is not a dataset",
                    e_name,
                )

                m_points_type = e_time["monotonic points"].dtype
                assert_validate(
                    m_points_type == np.int64,
                    f'The type of the dataset "monotonic points" should be np.int64 but was {m_points_type}',
                    e_name,
                )

                # 1 dimensional array
                assert_validate(
                    monotonic_points.ndim == 1,
                    "Dimensionality of monotonic points should be 3",
                    e_name,
                )

                # Either fixed in place or same length with poses
                assert_validate(
                    poses.shape[0] == 1 or monotonic_points.shape[0] == poses.shape[0],
                    "Monotonic points has to have the same length as poses (%d), but the length was %d. The entity is not fixed in place."
                    % (poses.shape[0], monotonic_points.shape[0]),
                    e_name,
                )

                # Either there is no outline, or fixed outline or same length with outline
                assert_validate(
                    outlines is None
                    or outlines.shape[0] == 1
                    or monotonic_points.shape[0] == outlines.shape[0],
                    "The specified outline has to have the length 1 (fixed outline) or the same length as monotonic points",
                    e_name,
                )

                assert_validate(
                    np.all(np.diff(monotonic_points) >= 0),
                    "Monotonic points is not monotonic",
                    e_name,
                )

            else:
                # Fixed in Place and fixed outline
                assert_validate(
                    poses.shape[0] == 1,
                    "There was no temporal definition (monotonic step or monotonic points) and the entity is not fixed in place",
                    e_name,
                )
                assert_validate(
                    outlines is None or outlines.shape[0] == 1,
                    "There was no temporal definition (monotonic step or monotonic points) and the entity does not have a fixed outline",
                    e_name,
                )

            # calendar points
            if "calendar points" in e_time:
                calendar_points = e_time["calendar points"]
                assert_validate(
                    calendar_points.ndim == 1,
                    "Dimensionality of calendar points should be 1",
                    e_name,
                )
                assert_validate(
                    calendar_points.shape[0] == monotonic_points.shape[0],
                    "The length of calendar points (%d) does not match the length of monotonic points (%d)"
                    % (calendar_points.shape[0], monotonic_points.shape[0]),
                    e_name,
                )

                # validate iso8601, this validates the dtype implicitly
                for c in calendar_points.asstr(encoding="utf-8"):
                    assert_validate(
                        validate_iso8601(c), "%s does not match iso8601" % c, e_name
                    )
    except Exception as e:
        if strict_validate:
            raise e
        else:
            logging.warning(e)
            return (False, e)
    return (True, "")


def validate_iso8601(str_val: str) -> bool:
    """This function validates strings to match the ISO8601 format.

    The source of the regex is https://stackoverflow.com/questions/41129921/validate-an-iso-8601-datetime-string-in-python

    Args:
        str_val (str): A string to be validated
    Returns:
        bool: validity of the string to iso8601
    """
    regex_iso8601 = r"^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])T(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])(\.[0-9]+)(Z|[+-](?:2[0-3]|[01][0-9]):[0-5][0-9])$"
    match_iso8601 = re.compile(regex_iso8601).match
    return match_iso8601(str_val) is not None
