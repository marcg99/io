import robofish.io
import datetime
from typing import Union, Iterable
from pathlib import Path
import logging

import numpy as np

# Optional pandas series support
list_types = (list, np.ndarray)
try:
    import pandas

    list_types += (pandas.core.series.Series,)
except ImportError:
    pass


def now_iso8061():
    return datetime.datetime.now(datetime.timezone.utc).isoformat(
        timespec="milliseconds"
    )


def read_multiple_files(
    paths: Union[Path, str, Iterable[Path], Iterable[str]], strict_validate=False
) -> dict:

    """ Load hdf5 files from a given path.

    TODO: Check if description is still correct
    The function can be given the path to a single single hdf5 file, to a folder,
    containing hdf5 files, or an array of multiple files or folders.

    Args:
        path (str): The path to a hdf5 file or folder.
    Returns:
        dict: A dictionary where the keys are filenames and the opened robofish.ioFile objects
    """

    logging.info(f"Reading files from path {paths}")

    if not isinstance(paths, list_types):
        paths = [paths]

    paths = [Path(p) for p in paths]

    sf_dict = {}

    for path in paths:
        if path.is_dir():
            logging.info("found dir %s" % path)
            # Find all hdf5 files in folder
            files = []
            for ext in ("hdf", "hdf5", "h5", "he5"):
                files += list(path.glob(f"*.{ext}"))
            files = sorted(files)

            logging.info("Reading files")

            for file in files:
                sf_dict.update(
                    {file: robofish.io.File(path=file, strict_validate=strict_validate)}
                )
        elif path is not None and path.exists():
            logging.info("found file %s" % path)
            sf_dict[path] = robofish.io.File(path=path, strict_validate=strict_validate)

    return sf_dict
