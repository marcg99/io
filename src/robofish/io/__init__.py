# SPDX-License-Identifier: LGPL-3.0-or-later

import sys
import logging

# TODO: REMOVE
logging.getLogger().setLevel(logging.INFO)

from robofish.io.file import File
from robofish.io.validation import *
from robofish.io.io import *
import robofish.io.app


assert (3, 7) <= sys.version_info < (4, 0), "Unsupported Python version"
