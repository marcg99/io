# -*- coding: utf-8 -*-

# -----------------------------------------------------------
# Utils functions for reading, validating and writing hdf5 files according to
# Robofish track format (1.0 Draft 7). The standard is available at
# https://git.imp.fu-berlin.de/bioroboticslab/robofish/track_format
#
# The term track is used to describe a dictionary, describing the track in a dict.
# To distinguish between attributes, dictionaries and groups, a prefix is used
# (a_ for attribute, d_ for dictionary, and g_ for groups).
#
# Dec 2020 Andreas Gerken, Berlin, Germany
# Released under GNU 3.0 License
# email andi.gerken@gmail.com
# -----------------------------------------------------------

import robofish.io
import h5py

import numpy as np

import logging
from typing import Iterable, Union
from pathlib import Path
import shutil

import datetime
import tempfile
import uuid
import pprint


temp_dir = tempfile.TemporaryDirectory()
default_trackformat_version = np.array([1, 0], dtype=np.int32)


class File(h5py.File):
    def __init__(
        self,
        path=None,
        world_size=None,
        version=default_trackformat_version,
        strict_validate=False,
    ):
        self._name = str(uuid.uuid4())
        self._tf_path = Path(temp_dir.name) / self._name
        self.load(path, strict_validate)

        if path is None:
            # Initialize new file
            assert world_size is not None and version is not None
            self.attrs["world size"] = np.array(world_size, dtype=np.float32)
            self.attrs["version"] = np.array(version, dtype=np.int32)
            self.create_group("entities")

    #### File Handling ####
    def load(self, path, strict_validate=False):
        if path and Path(path).exists():
            logging.info(f"Opening File {path}")
            self._f_path = Path(path)
            shutil.copyfile(self._f_path, self._tf_path)
            super().__init__(self._tf_path, "r+")
            self.validate(strict_validate)
        else:
            super().__init__(self._tf_path, "w")

    def save(self, path=None, bypass_validation=False):

        # Only valid files can be saved
        self.validate(strict_validate=not bypass_validation)

        if path is None:
            if self._f_path is None:
                raise Exception(
                    "path was not specified and there was no saved path from loading or an earlier save"
                )
        else:
            self._f_path = Path(path)

        self.close()

        if not self._f_path.parent.exists():
            self._f_path.parent.mkdir(parents=True, exist_ok=True)
        shutil.copyfile(self._tf_path, self._f_path)
        super().__init__(self._tf_path, "r+")

    def create_single_entity(
        self,
        type_,
        poses=None,
        name=None,
        outlines=None,
        monotonic_step=None,
        monotonic_points=None,
        calendar_points=None,
    ):
        if name is None:
            i = 1
            name = "%s_%d" % (type_, i)
            while name in self["entities"] and i < 10000:
                name = "%s_%d" % (type_, i)
                i += 1

        entity = self["entities"].create_group(name)
        entity.attrs["type"] = type_
        time = entity.create_group("time")

        # poses
        if poses is None:
            poses = np.empty((0, 4))
        entity.create_dataset("poses", data=poses, dtype=np.float32)

        # outlines
        if outlines is not None:
            entity.create_dataset("outlines", data=outlines, dtype=np.float32)

        # time
        if monotonic_step is not None:
            time.attrs["monotonic step"] = (np.int32)(monotonic_step)
        if monotonic_points is not None:
            time.create_dataset(
                "monotonic points", data=np.array(monotonic_points, dtype=np.int64)
            )
        if calendar_points is not None:

            def parse(p):
                if isinstance(p, datetime.datetime):
                    assert p.tzinfo is not None, "Missing timezone for calendar point."
                    return p.isoformat(timespec="milliseconds")
                elif isinstance(p, str):
                    assert p == datetime.datetime.fromisoformat(p).isoformat(
                        timespec="milliseconds"
                    )
                    return p
                else:
                    assert (
                        False
                    ), "Calendar points must be datetime.datetime instances or strings."

            calendar_points = [parse(p) for p in calendar_points]
            time.create_dataset(
                "calendar points",
                data=calendar_points,
                dtype=h5py.string_dtype(encoding="utf-8"),
            )

        return name

    def create_multiple_entities(
        self,
        types,
        poses,
        names=None,
        outlines=None,
        monotonic_step=None,
        monotonic_points=None,
        calendar_points=None,
    ):

        # Possibilities for input shapes:
        # type_ = str or (n, str)
        # poses = (n, timesteps, 4)
        # outlines = (timesteps,points,2) or (n, timesteps, points, 2)
        # monotonic_step= 1 or (n,)
        # monotonic_points = (timesteps,) or (n, timesteps)
        # calendar_points = (timesteps,) or (n, timesteps)

        def unpack_for_entity(thing, i, normal_dimensions):
            if thing is None:
                return None
            dim = 0 if not isinstance(thing, (list, np.ndarray)) else thing.ndim
            return thing if dim == normal_dimensions else thing[i]

        assert poses.ndim == 3
        assert poses.shape[2] == 4
        n = poses.shape[0]
        timesteps = poses.shape[1]
        returned_names = []

        types = types if isinstance(types, list) else [types] * n
        for i in range(n):

            e_name = unpack_for_entity(names, i, 0)
            e_outline = unpack_for_entity(outlines, i, 3)
            e_m_step = unpack_for_entity(monotonic_step, i, 0)
            e_m_points = unpack_for_entity(monotonic_points, i, 1)
            e_c_points = unpack_for_entity(calendar_points, i, 1)

            returned_names.append(
                self.create_single_entity(
                    types[i],
                    poses[i],
                    e_name,
                    e_outline,
                    e_m_step,
                    e_m_points,
                    e_c_points,
                )
            )
        return returned_names

    def get_entity_names(self):
        return sorted(self["entities"].keys())

    def get_poses_array(self, names=None, type_=None):
        # TODO: An issue might be, that the timing of entities might differ
        # The common pose array suggests, that they are comparable, when they
        # are not. At least a warning should be produced.

        if names is not None and type_ is not None:
            logging.error("Specify either names or types, not both.")

        # collect the names of all entities with the correct type
        if type_ is not None:
            names = [
                e_name
                for e_name, e_data in self["entities"].items()
                if e_data.attrs["type"] == type_
            ]

        # If no names or types are given, select all
        if names is None:
            names = self["entities"].keys()

        n = len(names)

        timesteps = (
            0
            if n == 0
            else max([self["entities"][e_name]["poses"].shape[0] for e_name in names])
        )

        # Initialize poses output array
        poses_output = np.empty((n, timesteps, 4))
        poses_output[:] = np.nan

        # Fill poses output array
        i = 0
        for e_name, e_data in sorted(self["entities"].items()):
            if e_name in names:
                poses_length = e_data["poses"].shape[0]
                poses_output[i][:poses_length] = e_data["poses"]
                i += 1
        return poses_output

    def validate(self, strict_validate=True):
        return robofish.io.validate(self, strict_validate)

    def to_string(self, output_format="shape"):
        def recursive_stringify(obj, output_format, level=0):
            s = ""
            level_str = "|---" * level
            for key, value in obj.attrs.items():
                s += "%s %s:\t%s\n" % (level_str, key, value)
            for key, value in obj.items():
                if isinstance(value, h5py.Dataset):
                    if output_format == "shape":
                        s += "%s %s:\t Shape %s\n" % (level_str, key, value.shape)
                    else:
                        s += "%s %s:\n%s\n" % (
                            level_str,
                            key,
                            np.array2string(
                                value, precision=2, separator=" ", suppress_small=True
                            ),
                        )
                if isinstance(value, h5py.Group):
                    s += "%s| %s\n%s" % (
                        level_str,
                        key,
                        recursive_stringify(value, output_format, level + 1),
                    )
            return s

        return recursive_stringify(self, output_format)

    def __str__(self):
        return self.to_string()


def now_iso8061():
    return datetime.datetime.now(datetime.timezone.utc).isoformat(
        timespec="milliseconds"
    )


def read_multiple_files(
    paths: Union[Path, str, Iterable[Path], Iterable[str]], strict_validate=False
) -> dict:

    """Load hdf5 files from a given path.

    TODO: Check if description is still correct
    The function can be given the path to a single single hdf5 file, to a folder,
    containing hdf5 files, or an array of multiple files or folders.

    Args:
        path (str): The path to a hdf5 file or folder.
    Returns:
        dict: A dictionary where the keys are filenames and the opened robofish.io.File objects
    """

    logging.info(f"Reading files from path {paths}")

    try:
        iter(paths)
    except TypeError:
        paths = [paths]
    paths = [Path(p) for p in paths]

    sf_dict = {}

    for path in paths:
        if path.is_dir():
            # Find all hdf5 files in folder
            files = []
            for ext in ("hdf", "hdf5", "h5", "he5"):
                files += list(path.glob(f"*.{ext}"))
            files = sorted(files)

            logging.info("Reading files")

            for file in files:
                sf_dict.update(
                    {file: robofish.io.File(path=file, strict_validate=strict_validate)}
                )
        elif path is not None:
            sf_dict[path] = robofish.io.File(path=path, strict_validate=strict_validate)

    return sf_dict
