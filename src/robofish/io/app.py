# -*- coding: utf-8 -*-

# -----------------------------------------------------------
# Functions available to be used in the commandline to show and validate hdf5
# according to the Robofish track format (1.0 Draft 7). The standard is
# available at https://git.imp.fu-berlin.de/bioroboticslab/robofish/track_format
#
# Dec 2020 Andreas Gerken, Berlin, Germany
# Released under GNU 3.0 License
# email andi.gerken@gmail.com
# -----------------------------------------------------------

import robofish.io
import argparse
import logging


def print(args=None):
    """This function can be used to print hdf5 files from the command line

    Returns:
        A human readable print of a given hdf5 file.
    """
    parser = argparse.ArgumentParser(
        description="This function can be used to print hdf5 files from the command line"
    )

    parser.add_argument("path", type=str, help="The path to a hdf5 file")
    parser.add_argument(
        "--output_format",
        type=str,
        choices=["shape", "full"],
        default="shape",
        help="Choose how datasets are printed, either the shapes or the full content is printed",
    )

    if args is None:
        args = parser.parse_args()

    sf = robofish.io.File(path=args.path, strict_validate=False)

    output_str = sf.to_string(args.output_format)
    output_str += "\n"
    output_str += (
        "Valid file" if sf.validate(strict_validate=False)[0] else "Invalid file"
    )
    return output_str


def validate(args=None):
    """This function can be used to validate hdf5 files.

    The function can be directly accessed from the commandline and can be given
    any number of files or folders. The function returns the validity of the files
    in a human readable format or as a raw output.

    Returns:
        A human readable table of each file and its validity
    """
    parser = argparse.ArgumentParser(
        description="The function can be directly accessed from the commandline and can be given any number of files or folders. The function returns the validity of the files in a human readable format or as a raw output."
    )
    parser.add_argument(
        "--output_format",
        type=str,
        default="h",
        choices=["h", "raw"],
        help="Output format, can be either h for human readable or raw for a dict.",
    )
    parser.add_argument(
        "path",
        type=str,
        nargs="+",
        help="The path to one or multiple files and/or folders.",
    )

    if args is None:
        args = parser.parse_args()

    logging.getLogger().setLevel(logging.ERROR)

    sf_dict = robofish.io.read_multiple_files(args.path)

    if args.output_format == "raw":
        sf_dict = {
            (str)(f): sf.validate(strict_validate=False)[0] for f, sf in sf_dict.items()
        }
        return sf_dict

    max_filename_width = max([len((str)(f)) for f in sf_dict.keys()])
    text = ""
    for file, sf in sf_dict.items():
        filled_file = (str)(file).ljust(max_filename_width + 3)
        validity, validity_message = sf.validate(strict_validate=False)
        text += f"{filled_file}:{validity}\t{validity_message}\n"

    return text
