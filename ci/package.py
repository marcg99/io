#! /usr/bin/env python3
# SPDX-License-Identifier: LGPL-3.0-or-later

from os import environ as env
from platform import system
from subprocess import check_call
from sys import executable


def python_executable():
    if system() == "Windows":
        return executable
    elif system() == "Linux":
        return env["PYTHON_EXECUTABLE"]
    assert False


if __name__ == "__main__":
    command = [python_executable()]
    command += ["setup.py", "bdist_wheel"]

    check_call(command)
