#! /usr/bin/env python3
# SPDX-License-Identifier: LGPL-3.0-or-later

from os import environ as env
from subprocess import check_call
from pathlib import Path
from platform import system


if __name__ == "__main__":
    if system() != "Linux":
        raise Exception("Uploading python package only supported on Linux")

    env["TWINE_USERNAME"] = "gitlab-ci-token"
    env["TWINE_PASSWORD"] = env["CI_JOB_TOKEN"]

    command = ["python3"]
    command += ["-m", "twine", "upload", "dist/*"]
    command += [
        "--repository-url",
        f"https://git.imp.fu-berlin.de/api/v4/projects/{env['CI_PROJECT_ID']}/packages/pypi",
    ]

    check_call(command)
