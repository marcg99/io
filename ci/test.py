#! /usr/bin/env python3
# SPDX-License-Identifier: LGPL-3.0-or-later

from os import environ as env
from subprocess import check_call
from platform import system
from sys import executable


def python_executable():
    if system() == "Windows":
        return executable
    elif system() == "Linux":
        return env["PYTHON_EXECUTABLE"]
    assert False


if __name__ == "__main__":
    check_call([python_executable(), "-m", "pip", "install", "pytest"])
    # check_call([python_executable(), "-m", "pip", "install", "pytest-cov"])
    check_call([python_executable(), "-m", "pip", "install", "h5py"])

    command = [python_executable()]
    command += ["-m", "pytest", "--junitxml=report.xml"]

    check_call(command)
